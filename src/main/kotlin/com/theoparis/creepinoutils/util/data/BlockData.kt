import com.theoparis.creepinoutils.util.data.INBTSerializable
import net.minecraft.block.Block
import net.minecraft.block.BlockState
import net.minecraft.block.Blocks
import net.minecraft.nbt.CompoundTag
import net.minecraft.nbt.NbtHelper
import net.minecraft.util.math.BlockPos
import net.minecraft.world.World

open class BlockData : INBTSerializable<Any?, CompoundTag> {
    var state: BlockState = Blocks.AIR.defaultState
    var position: BlockPos = BlockPos.ORIGIN
    var world: World? = null

    constructor(world: World, pos: BlockPos, state: BlockState) : this(pos, state) {
        this.world = world
    }

    constructor(world: World) {
        this.world = world
    }

    constructor(pos: BlockPos, state: BlockState) {
        position = pos
        this.state = state
    }

    /**
     * Helper method to get the block from the state stored in the data container
     */
    val block: Block
        get() = state.block

    override fun toTag(tag: CompoundTag): CompoundTag {
        tag.put("block", NbtHelper.fromBlockState(this.state))
        tag.put("position", NbtHelper.fromBlockPos(position))
        return tag
    }

    override fun fromTag(state: Any?, tag: CompoundTag): BlockData {
        this.state = NbtHelper.toBlockState(tag.getCompound("block"))
        position = NbtHelper.toBlockPos(tag.getCompound("position"))
        return this
    }
}
