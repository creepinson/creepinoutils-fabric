package com.theoparis.creepinoutils.util.data

import net.minecraft.nbt.Tag

interface INBTSerializable<S, T : Tag> {
    fun fromTag(state: S, tag: T): INBTSerializable<S, T>
    fun toTag(tag: T): T
}
