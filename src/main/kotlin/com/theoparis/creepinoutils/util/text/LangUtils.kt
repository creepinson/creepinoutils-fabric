package com.theoparis.creepinoutils.util.text

import net.minecraft.client.resource.language.I18n
import net.minecraft.text.MutableText
import net.minecraft.text.TextColor
import net.minecraft.text.TranslatableText
import net.minecraft.util.Formatting
import java.util.*

object LangUtils {
    fun transOnOff(b: Boolean): String {
        return localize(transOnOffKey(b))
    }

    private fun transOnOffKey(b: Boolean): String {
        return "gui." + if (b) "on" else "off"
    }

    fun transYesNo(b: Boolean): String {
        return localize("tooltip." + if (b) "yes" else "no")
    }

    fun transOutputInput(b: Boolean): String {
        return localize("gui." + if (b) "output" else "input")
    }
    /*     public static String localizeFluidStack(FluidStack fluidStack) {
        return fluidStack == null ? null : fluidStack.getFluid().getloc(fluidStack);
    } */
    /**
     * Localizes the defined string.
     *
     * @param s - string to localized
     * @return localized string
     */
    fun localize(s: String): String {
        return I18n.translate(s)
    }

    fun canLocalize(s: String): Boolean {
        return I18n.hasTranslation(s)
    }

    fun localizeWithFormat(key: String, vararg format: Any?): String {
        val s = localize(key)
        return try {
            String.format(s, *format)
        } catch (e: IllegalFormatException) {
            "Format error: $s"
        }
    }

    fun translationWithColour(langKey: String, color: Formatting): TranslatableText {
        val translation = TranslatableText(langKey)
        translation.style = translation.style.withColor(TextColor.fromFormatting(color))
        return translation
    }

    fun <T : MutableText> withColor(component: T, color: Formatting): T {
        component.style = component.style.withColor(TextColor.fromFormatting(color))
        return component
    }

    fun onOffColoured(isOn: Boolean): TranslatableText {
        val translation = TranslatableText(transOnOffKey(isOn))
        translation.style = translation.style.withColor(
            TextColor.fromFormatting(if (isOn) Formatting.DARK_GREEN else Formatting.DARK_RED)
        )
        return translation
    }
}
