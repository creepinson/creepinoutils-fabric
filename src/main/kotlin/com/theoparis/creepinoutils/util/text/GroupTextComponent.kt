package com.theoparis.creepinoutils.util.text

import net.minecraft.text.MutableText
import net.minecraft.text.Text
import net.minecraft.text.TranslatableText

/**
 * Originally created by Thiakil.
 * This is a utility class to make text components fluent.
 *
 * @author Theo Paris
 */
class GroupTextComponent : TranslatableText {
    constructor() : super("")
    constructor(color: EnumColor) : super("") {
        style = style.withColor(color.formatting)
    }


    override fun copy(): GroupTextComponent {
        val stc = GroupTextComponent()
        stc.style = style
        for (component in getSiblings()) {
            stc.append(component.copy())
        }
        return stc
    }

    fun string(s: String): GroupTextComponent {
        this.append(TranslatableText(s))
        return this
    }


    fun string(s: String, color: EnumColor): GroupTextComponent {
        val t = TranslatableText(s)
        t.style = t.style.withColor(color.formatting)
        this.append(t)
        return this
    }

    fun translation(key: String): GroupTextComponent {
        this.append(TranslatableText(key))
        return this
    }

    fun translation(key: String, vararg args: Any): GroupTextComponent {
        this.append(TranslatableText(key, *args))
        return this
    }

    fun translation(key: String, color: EnumColor): GroupTextComponent {
        val t = TranslatableText(key)
        t.style = t.style.withColor(color.formatting)
        this.append(t)
        return this
    }

    fun translation(key: String, color: EnumColor, vararg args: Any): GroupTextComponent {
        val t = TranslatableText(key, *args)
        t.style = t.style.withColor(color.formatting)
        this.append(t)
        return this
    }

    fun component(component: Text): GroupTextComponent {
        this.append(component)
        return this
    }

    fun component(component: MutableText, color: EnumColor): GroupTextComponent {
        component.style = component.style.withColor(color.formatting)
        this.append(component)
        return this
    }
}
