package com.theoparis.creepinoutils.util.api
/**
 * @author Theo Paris https://theoparis.com
 * Used to tell the difference between a normal player and a fake one.
 */
interface IFakePlayer
