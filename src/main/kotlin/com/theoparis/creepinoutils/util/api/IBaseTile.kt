package com.theoparis.creepinoutils.util.api

import com.theoparis.creepinoutils.util.upgrade.IUpgradeable
import net.minecraft.util.Tickable
import net.minecraft.util.math.BlockPos

interface IBaseTile : IConnectable, IRefreshable, IUpgradeable, Tickable {
    val isActive: Boolean
    override fun refresh()
    fun onNeighborChange(pos: BlockPos?)
}
