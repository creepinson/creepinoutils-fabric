package com.theoparis.creepinoutils.util.api

import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.Direction
import net.minecraft.world.WorldAccess

/**
 * @author Theo Paris https://theoparis.com
 * Project creepinoutils
 */
interface IConnectable {
    fun canConnectTo(blockAccess: WorldAccess, pos: BlockPos, side: Direction?): Boolean
    fun canConnectToStrict(blockAccess: WorldAccess, pos: BlockPos, side: Direction?): Boolean
}
